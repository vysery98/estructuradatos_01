import java.io.File;
import java.util.*;

/*
 * UNIVERSIDAD TÉCNICA PARTICULAR DE LOJA
 * INGENIERIA EN CIENCIAS DE LA COMPUTACION
 * ESTRUCTURA DE DATOS
 * __________________________________________
 * LUIS FERNANDO QUIZHPE ESPINOSA
 */
public class lfquizhpe1_GestionFilesCSV {

    public static void main(String[] args) {

        try {

            // Proceso de Generación de Archivo CSV con lso datos de los estudiantes
            Locale ingles = new Locale("en", "EN");
            Formatter outFile1 = new Formatter("lfquizhpe1BdEst_1.csv", "US-ASCII", ingles);
            String[] sepData1, sepData2, alumnos = {"TENE S. MARIA", "FAGGIONI L. GIOVANNI", "LOPEZ Q. JAIME",
                    "AVILA D. GREYS", "LUZURIA" + "GA C. DIANA", "MENDEZ C. JANETH", "CAMPOS D. FRANCISCA", "BORJA A." +
                    " ANDREA", "MENDOZA J. FATIMA", "SANCHEZ B. CARLA", "CASTILLO H. NESTOR", "SOLORZANO T. MARIBEL"};
            double[][] calif = new double[alumnos.length][13];
            String msgAlerta = "", cadena = "";
            double fin1, fin2, total, aprob = 0, reprob = 0, final1 = 0, final2 = 0, final1_2 = 0, trabajo = 0;
            int aux = 0;
            int[] finales = new int[alumnos.length];
            ArrayList<String> data = new ArrayList<>(), data2 = new ArrayList<>();

            // Generación de calificaciones
            for (int i = 0; i < alumnos.length; i++) {
                for (int j = 0; j < calif[i].length; j++) {
                    calif[i][j] = (j <= 2 || (j > 4 && j < 8)) ? (Math.random() * 1.001) : (j == 3 || j == 8)
                            ? (Math.random() * 6.001) : (j == 4 || j == 9) ? (Math.random() * 14.001) : (j == 10)
                            ? 0 : (j == 11) ? 0 : (calif[i][0] + calif[i][1] + calif[i][2] + calif[i][3] + calif[i][4] +
                            calif[i][5] + calif[i][6] + calif[i][7] + calif[i][8] + calif[i][9]);
                }
            }

            // Generación de cadena
            for (int i = 0; i < alumnos.length; i++) {
                cadena = String.format("%s%s;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f\n", cadena,
                        alumnos[i], calif[i][0], calif[i][1], calif[i][2], calif[i][3], calif[i][4], calif[i][5],
                        calif[i][6], calif[i][7], calif[i][8], calif[i][9], calif[i][10], calif[i][11], calif[i][12]);
            }

            // Inclusión de cadena en csv
            outFile1.format(cadena);
            // Cierre y generación de archivo
            outFile1.close();

            // Proceso de lectura de archivo
            Scanner inFile1 = new Scanner(new File("lfquizhpe1BdEst_1.csv"));
            // Escritura de nuevo archivo con recalificación
            Formatter outFile2 = new Formatter("lfquizhpe1BdEst_2.csv", "US-ASCII", ingles);

            while (inFile1.hasNext()) {
                sepData1 = inFile1.nextLine().replace(",", ".").split(";");
                // Variables auxiliares para volver a calcular o generar los datos.
                total = 0;
                fin1 = 0;
                fin2 = 0;

                if (Double.valueOf(sepData1[1]) == 0 && Double.valueOf(sepData1[2]) == 0 && Double.valueOf(sepData1[3]) == 0
                        && Double.valueOf(sepData1[4]) == 0 && Double.valueOf(sepData1[5]) == 0 &&
                        Double.valueOf(sepData1[6]) == 0 && Double.valueOf(sepData1[7]) == 0 && Double.valueOf(sepData1[8]) == 0
                        && Double.valueOf(sepData1[9]) == 0 && Double.valueOf(sepData1[10]) == 0) {
                    msgAlerta = "Reprobado Falta Trabajo";
                    trabajo++;
                    total = 0;
                }

                if (Double.valueOf(sepData1[5]) < 8 && Double.valueOf(sepData1[10]) < 8) {
                    msgAlerta = "Rendir Final 1 y 2";
                    fin1 = Math.random() * 20.001;
                    fin2 = Math.random() * 20.001;
                    total = fin1 + fin2;
                    final1_2++;

                } else {
                    if (Double.valueOf(sepData1[5]) < 8) {
                        msgAlerta = "Rendir Final 1";
                        fin1 = (Math.random() * 20.001);
                        total = fin1 + Double.valueOf(sepData1[6]) + Double.valueOf(sepData1[7]) + Double.valueOf(sepData1[8])
                                + Double.valueOf(sepData1[9]) + Double.valueOf(sepData1[10]);
                        final1++;
                    }
                    if (Double.valueOf(sepData1[10]) < 8) {
                        msgAlerta = "Rendir Final 2";
                        fin2 = (Math.random() * 20.001);
                        total = Double.valueOf(sepData1[1]) + Double.valueOf(sepData1[2]) + Double.valueOf(sepData1[3])
                                + Double.valueOf(sepData1[4]) + Double.valueOf(sepData1[5]) + fin2;
                        final2++;
                    }
                }

                total = (total > 40) ? 40 : total;
                String promocion = (total >= 27.50) ? "Aprobado" : "Reprobado";

                outFile2.format("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%.2f;%.2f;%.0f;%s;%s\n", sepData1[0],
                        sepData1[1], sepData1[2], sepData1[3], sepData1[4], sepData1[5], sepData1[6], sepData1[7],
                        sepData1[8], sepData1[9], sepData1[10], fin1, fin2, total, msgAlerta, promocion);


            }
            // Cierre de archivo
            inFile1.close();
            outFile2.close();

            // Proceso de lectura de archivo
            Scanner inFile2 = new Scanner(new File("lfquizhpe1BdEst_2.csv"));

            Formatter outFileOrder1 = new Formatter("lfquizhpe1BdEst_OrdenNomb.csv", "US-ASCII", ingles);
            Formatter outFileOrder2 = new Formatter("lfquizhpe1BdEst_OrdenTOTAL.csv", "US-ASCII", ingles);
            // Formatter outFileOrder3 = new Formatter("lfquizhpe1BdEst_OrdenPromo.csv", "US-ASCII", ingles);

            // array usado para separar las notas finales de los estudiantes
            while (inFile2.hasNext()) {
                cadena = inFile2.nextLine();
                String[] tokens = cadena.split(";");
                finales[aux++] = Integer.valueOf(tokens[13]);
                data.add(cadena);
            }
            Collections.sort(data);
            Iterator dataSet = data.iterator();

            while (dataSet.hasNext()){
                cadena = (String) dataSet.next();
                outFileOrder1.format("%s\n", cadena);
            }

            // Cierre de archivo
            inFile2.close();
            outFileOrder1.close();
            outFileOrder2.close();

            // Salida por consola
            System.out.printf("PORCENTAJE DE ESTUDIANTES:\nAPROBADOS = %.0f %% \nREPROBADOS = %.0f %% \nFINAL 1 = " +
                            "%.0f %% \nFINAL 2 = %.0f %% \nFINAL 1 y 2 = %.0f %%\nFALTA TRABAJO = %.0f %%",
                    (aprob * 100) / alumnos.length, (reprob * 100) / alumnos.length, (final1 * 100) / alumnos.length,
                    (final2 * 100) / alumnos.length, (final1_2 * 100) / alumnos.length, (trabajo * 100) / alumnos.length);
        } catch (Exception e) {

            System.out.println("ERROR -> " + e);
        }
    }
}
